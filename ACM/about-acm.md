#About UAB ACM
[The Association of Computing Machinery (ACM)](http://www.acm.org/) is the world’s oldest and largest scientific computing society. The UAB ACM is the ACM’s local student chapter for the UAB community. It has been active continuously since 1984, providing UAB’s computer science and related students and professionals with educational and social opportunities.

The UAB ACM is charged with organizing activities ranging from distinguished CIS Seminar Series lecturers, programming contests and outreach efforts to social events such as movie nights, camping trips and other gatherings. The students determine the activities of the chapter. UAB ACM holds regular business and social meetings and sponsors and co-sponsors numerous events throughout the year.

####UAB ACM’s specific goals are too:
- Promote an increased knowledge of the science, design, development, construction, languages, and applications of modern computing technology.
- Promote a greater awareness of computing machinery and its applications at national and local levels.

- Provide for professional, academic, and social interactions among persons having interests in computing machinery.

 

####Current Executive Officers of the ACM (2014 – 2015)
|  Position  |   Position Holder   |    Email        |
| ---------- | ------------------- | --------------- |
| Chair      | Andrea Beam         | apbeam@uab.edu  | 
| Vice Chair | Erby Fischer        | erbycf@uab.edu  | 
| Treasurer  | Tarra Kuhn          | tarrak@uab.edu  |
| Secretary  | Stephen Stucky      | stucky3@uab.edu |

