#CIS Unoffical Handbook

##Index
- [Getting Started](getting-started/)   
    - [Who's Who](getting-started/whos-who.md)  
    - [Card Access](getting-started/access.md)  
- [User Account & Usage](user-account/)  
- [Software](software/)  
    - [UAB Software]()
    - [DreamSpark/MS-DNAA]()
- [Network Services](network-services/)  
    - [Email](network-services/email.md)  
    - [Home Directory](network-services/home-directory.md)  
    - [Personal Webspace](network-services/personal-webspace.md)  
    - [Gitlab](network-services/gitlab.md)  
    - [Vulan Servers](network-services/vulcan-servers.md)  
- [CIS Lab](uGradLab/)  
    - [Card Access](getting-started/access.md)  
    - [Printers]()
        - [How to Install Printer on Personal Laptop](uGradLab/printer-how-to.md)  
- [Association for Computing Machinery (ACM)](ACM/about-acm.md)  

## ACM Turtoring
|      Day      |      Hours        |     Tutor     | Classes                                |
| ------------  | ----------------- | ------------- | -------------------------------------- |
|   Monday      | 1:00pm – 2:15pm   | Daniel Latham | 100, 200, 201, 250, 302, 303           |
|   Tuesday     | 8:00am – 9:15am   | Amalee Wilson | 100, 200, 201, 250, 302, 303, 330, 350 |
|   Wednesday   | 10:50am – 12:05pm | Daniel Latham | 100, 200, 201, 250, 302, 303           |
|   Thursday    | 8:00am – 9:15am   | Amalee Wilson | 100, 200, 201, 250, 302, 303, 330, 350 |
|   Friday      | 9:30am – 12:00pm  | Ryan McConn   | 100, 200, 201, 250, 302, 303, 330, 350 |
