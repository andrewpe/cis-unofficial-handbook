#How to install printer (Ethernet Only) 
##Windows
Step 1. Go to Control Panel  
Step 2. Click Hardware and Sound  
Step 3. Undernether Devices and Printers, click Advanced printer setup   
Step 4. Click The printer that I want isn't listed  
Step 5. Select Add a printer using a TCP/IP address or hostname  
Step 6. Device type: Autodetect  Hostname or IP address: 10.13.2.122  Port name: autofill  

##OS X
Step 1. Go to System Preferences  
Step 2. Click Printer & Scanners  
Step 3. Click +   
Step 4. Select IP Tab  
Step 5. In the Address Field enter: 10.13.2.122  
Step 6. Click Add   
Step 7. Click Continue   
(Optional) Enter a nickname into the Name field  
(Optional) Enter a location nickname into the Location field    

##Linux
Step 1. Figure it out yourself  
Step 2. Print
