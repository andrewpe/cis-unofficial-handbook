#CIS Campus Access
##How to Request Access Access
Step 1. Talk to Janet Tatum for a Access Request Form  
Step 2. Fill out form  
Step 3. Take form to UAB One Stop  

##Privileges of Access
- **Campbell Hall** (after hours) (CIS Students)
- **Undergraduate Lab CH154/154A/154B** (CIS Students)
- **Graduate Lab CH135/135A** (CIS Graduate Students)
- **Teach Labs CH145/CH137A/CH430/CH435** (CIS Teaching Assistants)
- **Research Labs** (CIS Students assigned to lab)